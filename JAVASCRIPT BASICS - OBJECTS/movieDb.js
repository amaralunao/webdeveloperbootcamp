console.log('CONNECTED')

movies = [
	{
		title: "In Bruges",
		rating: 5,
		hasWatched: true,
	},
	{
		title: "Frozen",
		rating: 4.5,
		hasWatched: false,
	},
	{
		title: "MAD MAX FURY ROAD",
		rating: 5,
		hasWatched: true,
	},
	{
		title: "Les Miserables",
		rating: 3.5,
		hasWatched: false,
	},
]

function buildString(movie){
	var text = "You have "
	if (movie.hasWatched){
		text += "watched ";
	} else {
		text += "not seen ";
	}
	text += "\"" + movie.title + "\" - "
	text += movie.rating + " stars"
	return text;
}

function displayMovies(array) {
	array.forEach(function(movie){
		console.log(buildString(movie));
	});
}

displayMovies(movies);