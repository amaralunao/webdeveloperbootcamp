  function isEven(num) {
  	 return num % 2 === 0;
  }


  function factorialRecursive(num) {
  	if(num === 0) {
  		return 1;
  	}
  	return num*factorial(num-1);
  }

  function factorial(num) {
  	var result = 1;
  	for(var i = 2; i <= num; i++) {
  		result *= i;
  	}
  	return result;
  }

  function kebabToSnake(str) {
  	var newStr = return str.replace(/-/g, "_");
  	return newStr;
  }